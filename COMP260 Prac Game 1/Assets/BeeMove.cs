﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeMove : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}

    public float speed = 4.0f; // metres per second
    public float turnSpeed = 180.0f; // degrees per second
    public Transform target1;
    public Transform target2;
    private Vector2 heading = Vector2.right;

    // Update is called once per frame
    void Update () {
        // get the vector from the bee to the target
        Vector2 direction1 = target1.position - transform.position;
        Vector2 direction2 = target2.position - transform.position;
        // calculate how much to turn per frame
        float angle = turnSpeed * Time.deltaTime;




        if (direction1.magnitude < direction2.magnitude)
        {
            heading = direction1;
            // turn left or right
            if (direction1.IsOnLeft(heading))
            {
                // target on left, rotate anticlockwise
                heading = heading.Rotate(angle);
            }
            else
            {
                heading = direction1;
                // target on right, rotate clockwise
                heading = heading.Rotate(-angle);
            }
            transform.Translate(heading * speed * Time.deltaTime);

        }
        else
        {
            heading = direction2;
            // turn left or right
            if (direction2.IsOnLeft(heading))
            {
                // target on left, rotate anticlockwise
                heading = heading.Rotate(angle);
            }
            else
            {
                heading = direction2;
                // target on right, rotate clockwise
                heading = heading.Rotate(-angle);
            }
            transform.Translate(heading * speed * Time.deltaTime);

        }
        


    }

    void OnDrawGizmos()
    {
        // draw heading vector in red
        Gizmos.color = Color.red;
        Gizmos.DrawRay(transform.position, heading);
        // draw target vector in yellow
        Gizmos.color = Color.yellow;
        Vector2 direction = target1.position - transform.position;
        Gizmos.DrawRay(transform.position, direction);
    }
}
