﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
    public float maxSpeed = 5.0f; // in metres per second
    public float acceleration = 3.0f; // in metres/second/second
    private float speed = 0.0f; // in metres/second
    public float brake = 5.0f; // in metres/second/second
    public float turnSpeed = 30.0f; // in degrees/second
    public float brakeTarget = 10.0f; // maximum brake value

    void Update()
    {
        // the horizontal axis controls the turn
        float turn = Input.GetAxis("Horizontal");
        // turn the car

        if(speed < 2.5)
        {
            turnSpeed = 15;
        } else
        {
            if(speed > 2.5)
            {
                turnSpeed = 40;
            }
        }

        transform.Rotate(0, 0, -turn * turnSpeed * Time.deltaTime);

        // the vertical axis controls acceleration fwd/back
        float forwards = Input.GetAxis("Vertical");
        if (forwards > 0)
        {
            // accelerate forwards
            speed = speed + acceleration * Time.deltaTime;
        }
        else if (forwards < 0)
        {
            // accelerate backwards
            speed = speed - acceleration * Time.deltaTime;
        }
        else
        {
            if(brake > brakeTarget)
            {
                brake = brakeTarget;
            }
            // braking
            if (speed > 0) //use a clamp or a min max
            {
           if (brake * Time.deltaTime > Mathf.Abs(speed))
                {
                    speed = 0;
                }
           else
                {
                    speed = speed - brake * Time.deltaTime;
                }
                
            }

            else
            {
                

                if (brake * Time.deltaTime > Mathf.Abs(speed))
                {
                    speed = 0;
                }

                else
                {
                    speed = speed + brake * Time.deltaTime;
                }
            }

        }

        // clamp the speed
        speed = Mathf.Clamp(speed, -maxSpeed, maxSpeed);


        // compute a vector in the up direction of length speed
        Vector2 velocity = Vector2.up * speed;
        // move the object
        transform.Translate(velocity * Time.deltaTime, Space.Self);
    }

}
